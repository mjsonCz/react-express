var redis = require('redis');
var bcrypt = require('bcrypt');
var db = redis.createClient(6379,"127.0.0.1");
module.exports = User;

function User(obj) {
    for(var key in obj) {
        this[key] = obj[key];
    }
}
User.prototype.save = function(fn) {
    if(this.id) {
        this.update(fn);
    }else {
        var user = this;
        //对存储在指定key的数值执行原子的加1操作。如指定的key不存在，那在执行incr之前，先将它的值设为0。
        db.incr('user:ids', function(err, id) {
            if(err) return fn(err);
            user.id = id;
            user.hashPassword(function(err) {
                if(err) return fn(err);
                user.update(fn);
            });
        });
    }
};
User.prototype.update = function(fn) {
    var user = this; 
    var id = user.id;
    //将字符串值 value 关联到 key,如果 key 已经持有其他值， SET 就覆写旧值，无视类型。
    db.set('user:id:'+ user.name, id, function(err) {
        if(err) return fn(err);
        db.hmset('user:' + id, user, function(err) {
            fn(err);
        });
    });
};
User.prototype.hashPassword = function(fn) {
    var user = this;
    bcrypt.genSalt(12, function(err, salt) {
        if(err) return fn(err);
        user.salt = salt;
        bcrypt.hash(user.pass, salt, function(err, hash) {
            if(err) return fn(err);
            user.pass = hash;
            fn();
        });
    });
};

var tobi = new User({
    name: 'Tobi',
    pass: 'im a ferret',
    age: 2
});
tobi.save(function(err) {
    if(err) throw err;
    console.log('user id %d', tobi.id);
});